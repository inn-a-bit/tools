package tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ConvertJSONtoExcel {

	static Map<String, String> allRecordsMap = new TreeMap<String, String>();
	static XSSFWorkbook workbook;
	static XSSFSheet sheet;
	static Row row;
	static CellStyle style;
	static CellStyle styleRow;

	public static void main(String[] args) throws Exception {
		Scanner scanner = new Scanner(System.in);
		System.out.print(
				"Enter a fully qualified file name(ex: /path/to/your/file/menu.json) : \n");
		///Users/innakomarova/Documents/workspace/Tools/src/tools/menu.json
		String inputFileName = scanner.next();
		String excelFileName = inputFileName.substring(0, inputFileName.lastIndexOf(".json")) + ".xlsx";
		System.out.print("Data will be written to " + excelFileName + "\n");
		scanner.close();
		readJSONwriteToEXCEL(inputFileName, excelFileName);
	}

	private static void readJSONwriteToEXCEL(String jsonFileName, String excelFileName) throws Exception {
		Object object = new JSONParser().parse(new FileReader(jsonFileName));
		JSONObject jo = null;
		JSONArray ja = null;
		
		FileOutputStream file = new FileOutputStream(new File(excelFileName));
		workbook = new XSSFWorkbook();
		sheet = workbook.createSheet("JSONObject");		
		style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);		
		styleRow = workbook.createCellStyle();
		styleRow.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		styleRow.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		if (object instanceof JSONArray) {
			ja = (JSONArray) object;
			int jsonSize = ja.size();

			for (int i = 0; i < jsonSize; i++) {
				// create the header
				jo = (JSONObject) ja.get(i);
				createHeader(jo, "HHHHH");
			}
			row = sheet.createRow(0);
			writeHeaderToExcel(row);
			
			for (int i = 0; i < jsonSize; i++) {
				jo = (JSONObject) ja.get(i);
				
				for (Map.Entry<String, String> m : allRecordsMap.entrySet()) {
					allRecordsMap.replace((String) m.getKey(), "");
				}
				parseJSON(jo, "HHHHH");
				row = sheet.createRow(i + 1);
				writeRecordToExcel(row, i + 1);
			}
			workbook.write(file);
		} 
		else
		{
			createHeader((JSONObject) object, "HHHHH");
			row = sheet.createRow(0);
			writeHeaderToExcel(row);
			
			for (Map.Entry<String, String> m : allRecordsMap.entrySet()) {
				allRecordsMap.replace((String) m.getKey(), "");
			}
			parseJSON((JSONObject) object, "HHHHH");
			row = sheet.createRow(1);
			writeRecordToExcel(row, 1);		
			workbook.write(file);
		}
		workbook.close();
		file.close();
	}

	private static void writeHeaderToExcel(Row row) throws Exception{
		int cellCount = 0;
		Cell cell = null;

			for (Map.Entry<String, String> m : allRecordsMap.entrySet()) {
				cell = row.createCell(cellCount);
				cell.setCellValue(((String) m.getKey()).replaceAll("HHHHH.", ""));		
				row.getCell(cellCount).setCellStyle(style);
				cellCount++;
			}
	}

	private static void writeRecordToExcel(Row row, int ii) {
		int cellCount = 0;		
		Cell cell = null;
		for (Map.Entry<String, String> m : allRecordsMap.entrySet()) {
				
					if (ii % 2 == 0) {	
						cell = row.createCell(cellCount);
						cell.setCellValue((String) m.getValue());
						row.getCell(cellCount).setCellStyle(styleRow);
					} else {
						cell = row.createCell(cellCount);
						cell.setCellValue((String) m.getValue());
					}		
				cellCount++;
			}
	}
	private static void createHeader(JSONObject jo, String objectName) throws Exception {

		Set<String> setJson = jo.keySet();
		Iterator<String> itr = setJson.iterator();

		while (itr.hasNext()) {
			String fieldName = itr.next();
			
			// attribute
			if ((jo.get(fieldName) instanceof String) || (jo.get(fieldName) instanceof java.lang.Long)
					|| (jo.get(fieldName) instanceof java.lang.Double)
					|| (jo.get(fieldName) instanceof java.lang.Boolean) || (jo.get(fieldName) == null)) {
				objectName = objectName + "." + fieldName;
				allRecordsMap.put(objectName, "");
				objectName = objectName.substring(0, objectName.lastIndexOf("."));
			}
			// array
			else if (jo.get(fieldName) instanceof JSONArray) {
				JSONArray jsonArray = (JSONArray) jo.get(fieldName);
				if (!jsonArray.isEmpty() && (jsonArray.get(0) instanceof JSONObject)) // this is an array of objects
				{
					int index = 0;
					for (int i = 0; i < jsonArray.size(); i++) {
						index = i + 1;
						String tmp = "." + fieldName + index;
						objectName = objectName + tmp;
						createHeader((JSONObject) jsonArray.get(i), objectName);
						objectName = objectName.substring(0, objectName.lastIndexOf("."));					
					}
				} else if(jsonArray.isEmpty()) 
				{
					// empty array 
				}
				else // array of attributes
				{
					int index = 0;
					for (int i = 0; i < jsonArray.size(); i++) {
						index = i + 1;
						String tmp = "." + fieldName + index;
						objectName = objectName + tmp;
						
						allRecordsMap.put(objectName, "");
						objectName = objectName.substring(0, objectName.lastIndexOf("."));
					}
				}
			} else if (jo.get(fieldName) instanceof JSONObject) // JSON Object
			{
				String tmp = "." + fieldName;
				objectName = objectName + tmp;
				createHeader((JSONObject) jo.get(fieldName), objectName);
				objectName = objectName.substring(0, objectName.lastIndexOf("."));
			}
		}
	}

	private static void parseJSON(JSONObject jo, String objectName) throws Exception {
		Set<String> setJson = jo.keySet();
		Iterator<String> itr = setJson.iterator();
		String value = "";		
		while (itr.hasNext()) {
			String fieldName = itr.next();
			value = "";
			if ((jo.get(fieldName) instanceof String) || (jo.get(fieldName) instanceof java.lang.Long)
					|| (jo.get(fieldName) instanceof java.lang.Double)
					|| (jo.get(fieldName) instanceof java.lang.Boolean) || jo.get(fieldName) == null) // this is an array of attributes
			{
				if (jo.get(fieldName) == null) {
					value = "";
				}
				else if ((jo.get(fieldName) instanceof java.lang.Long)) {
					value = new java.lang.Long((Long) jo.get(fieldName)).toString();
				} else if ((jo.get(fieldName) instanceof java.lang.Boolean)) {
					value = new java.lang.Boolean((Boolean) jo.get(fieldName)).toString();
				} else if ((jo.get(fieldName) instanceof java.lang.Double)) {
					value = new java.lang.Double((java.lang.Double) jo.get(fieldName)).toString();

				} else {
					value = (String) jo.get(fieldName);
				}
				if (value != null && value.trim().equals("")) {
					value = "";
				}
				objectName = objectName + "." + fieldName;

				if(allRecordsMap.containsKey(objectName)){
					allRecordsMap.replace(objectName, value);
				}				
				objectName = objectName.substring(0, objectName.lastIndexOf("."));
			} else if (jo.get(fieldName) instanceof JSONArray) {

				JSONArray jsonArray = (JSONArray) jo.get(fieldName);

				if (!jsonArray.isEmpty() && (jsonArray.get(0) instanceof JSONObject)) // this is an array of objects
				{
					int index = 0;
					for (int i = 0; i < jsonArray.size(); i++) {
						index = i + 1;
						String tmp = "." + fieldName + index;
						objectName = objectName + tmp;
						parseJSON((JSONObject) jsonArray.get(i), objectName);
						objectName = objectName.substring(0, objectName.lastIndexOf("."));
						}
				} 
				else if (jsonArray.isEmpty() ) // this is an array of objects
				{
					// SKIP AN EMPTY ARRAY
				} 
				else // this is an array of attributes
				{
					int index = 0;
					value = "";
					for (int i = 0; i < jsonArray.size(); i++) {
						index = i + 1;
						String tmp = "." + fieldName + index;
						objectName = objectName + tmp;
						if (jsonArray.get(i) == null) {
							value = "";
						}

						else if ((jsonArray.get(i) instanceof java.lang.Long)) {
							value = new java.lang.Long((Long) jsonArray.get(i)).toString();
						} else if ((jsonArray.get(i) instanceof java.lang.Boolean)) {
							value = new java.lang.Boolean((Boolean) jsonArray.get(i)).toString();
						} else if ((jsonArray.get(i) instanceof java.lang.Double)) {
							value = new java.lang.Double((java.lang.Double) jsonArray.get(i)).toString();
						} else {
							value = (String) jsonArray.get(i);
						}
						if (value != null && value.trim().equals("")) {
							value = "";
						}
						value = (String) jsonArray.get(i);
						for (Map.Entry<String, String> keyValue : allRecordsMap.entrySet()) {
							String attrName = (String) keyValue.getKey();
							if (attrName.equals(objectName)) {
								allRecordsMap.replace(objectName, value);
							}
						}
						objectName = objectName.substring(0, objectName.lastIndexOf("."));
					}
				}

			} else // JSON Object
			{
				String tmp = "." + fieldName;
				objectName = objectName + tmp;
				parseJSON((JSONObject) jo.get(fieldName), objectName);
				objectName = objectName.substring(0, objectName.lastIndexOf("."));
			}
		}
	}
}
