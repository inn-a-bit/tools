
## Java Tools ##
### Generate Excel spread sheet from a JSON file. ###
#### For a given file name in JSON format, an Excel spread sheet will be generated. ####
#### Example of JSON payload: ####
```json
[
     {
          "user":"Lena",
          "skills":[
                       {
                          "skill":"Unix",
                          "years":"10",
                          "used":"2010",
                          "level":"Advanced",
                          "action":"Add"
                        },
                        {
                           "skill":"Selenium",
                           "years":"1",
                           "used":"2020",
                           "level":"Intermidiate",
                           "action":"Add"
                        }
                    ]
      },
      {
           "user":"Inna",
           "skills":[
                      {
                          "skill":"Java",
                          "years":"15",
                          "used":"2020",
                          "level":"Advanced",
                          "action":"Add"
                       },
					   {
                          "skill":"AWS",
                          "years":"1",
                          "used":"2020",
                          "level":"Intermidiate",
                          "action":"Add"
                        }
                  ]
        }
]
```
#### Which will be converted into Excel spread sheet: ####
![alt text](https://inn-a-cloud.s3.us-east-2.amazonaws.com/SkillsList.png)

